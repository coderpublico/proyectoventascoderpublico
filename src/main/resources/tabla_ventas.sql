CREATE TABLE ventas_ey (
id_cliente INT NOT NULL, 
cantidad INT NOT NULL, 
codigo_producto INT NOT NULL,
desc_producto VARCHAR(150) NOT NULL,
fecha DATE NOT NULL,
gasto_total INT NOT NULL,
nombre_cliente VARCHAR(150) NOT NULL,
FOREIGN KEY (id_cliente) REFERENCES cliente_ey(id)
);