package com.entrega.workshop.service.clientes;

import com.entrega.workshop.models.clientes.ClienteEy;
import com.entrega.workshop.repository.clientes.RepositoryClientes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceClientes {

    @Autowired
    private RepositoryClientes repoClientes;

    /**
     * Metodo para cargar cliente nuevo
     * @param cliente
     */
    public void postCliente(ClienteEy cliente) {
        repoClientes.save(cliente);
    }
    /**
     * mostrarClientes> muestra un json de todos los clientes en la base
     * @return
     */
    public List<ClienteEy> mostrarClientes(){
        List<ClienteEy> lista = repoClientes.findAll();
        return lista;
    }

    /**
     * updateClientes> Hace una actualizacion del id seleccionado en la URI
     * @param id
     * @param cliente
     */
    public void updateClientes(Long id, ClienteEy cliente){
        ClienteEy updateCliente = repoClientes.findById(id).get();
        updateCliente.setNombre(cliente.getNombre());
        updateCliente.setEmail(cliente.getEmail());
        repoClientes.save(updateCliente);
    }

}
