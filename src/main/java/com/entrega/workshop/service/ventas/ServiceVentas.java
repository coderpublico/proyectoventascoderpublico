package com.entrega.workshop.service.ventas;


import com.entrega.workshop.models.clientes.ClienteEy;
import com.entrega.workshop.models.entidad.CustomException;
import com.entrega.workshop.models.entidad.LineaEy;
import com.entrega.workshop.models.entidad.WorldClock;
import com.entrega.workshop.models.productos.ProductosEy;
import com.entrega.workshop.models.ventas.VentasEy;

import com.entrega.workshop.repository.clientes.RepositoryClientes;
import com.entrega.workshop.repository.entidad.RepositoryLinea;
import com.entrega.workshop.repository.productos.RepositoryProductos;
import com.entrega.workshop.repository.ventas.RepositoryVentas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

/**
 * Desarrollo de metodos para comprobante de ventas
 */
@Service
public class ServiceVentas {
    @Autowired
    private RepositoryVentas repoVentas;

    @Autowired
    private RepositoryProductos repoProductos;

    @Autowired
    private RepositoryClientes repoClientes;
    @Autowired
    private RepositoryLinea repoLinea;
    @Autowired
    private RestTemplate restTemplate;

    /**
     * mostrarVentas
     * muestra todas las ventas de la base de datos
     * @return
     */
    public List<VentasEy> mostrarVentas(){
        List<VentasEy> lista = repoVentas.findAll();
        return lista;
    }

    /**
     * muestra en Postman el comprobante de la venta especificada por Id
     * @param idVenta
     * @return
     */
    public VentasEy mostrarComprobanteDeVenta(Long idVenta){
        Optional<VentasEy> venta = repoVentas.findById(idVenta);
        if(venta.isPresent()){
            return venta.get();
        } else {
            throw new CustomException("No existe la venta solicitada.");
        }
    }

    /**
     * postVentas
     * metodo que carga los datos de la venta ejecutada
     * @param ventas
     * @return
     */
    public void postVentas(VentasEy ventas) {
        ProductosEy productos = repoProductos.findById(ventas.getIdProducto().getIdProducto()).orElseThrow(() -> new CustomException("Producto no encontrado"));
        ClienteEy cliente = repoClientes.findById(ventas.getIdCliente().getId()).orElseThrow(() -> new CustomException("Cliente no encontrado"));
        LineaEy linea = new LineaEy();
        WorldClock worldClock = this.restTemplate.getForObject("http://worldclockapi.com/api/json/utc/now", WorldClock.class);
        String currentDateTime = worldClock.getCurrentDateTime();

        if (productos.getStock() > ventas.getCantidad()) {
            ventas.setIdCliente(ventas.getIdCliente());
            ventas.setCantidad(ventas.getCantidad());
            ventas.setGastoTotal(ventas.getCantidad() * productos.getPrecio());
            try {
                Date fechaActual = new SimpleDateFormat("yyyy-MM-dd'T'mm:ss'Z'").parse(currentDateTime);
                ventas.setFecha(fechaActual);
            } catch (ParseException e) {
                e.printStackTrace();
                ventas.setFecha(new Date());
            }
            //Seteo la linea de detalles de la venta
            linea.setCantidad(ventas.getCantidad());
            linea.setDescripcion(productos.getDescripcion());
            linea.setPrecio(productos.getPrecio());
            //Actualizo la cantidad de productos
            productos.setStock(productos.getStock() - ventas.getCantidad());

            if (ventas.getLineas() == null) {
                ventas.setLineas(new HashSet<>());
            }
            ventas.getLineas().add(linea);
            repoVentas.save(ventas);
            linea.setComprobanteDeVenta(ventas);
            repoLinea.save(linea);
            repoProductos.save(productos);

        } else {
            throw new CustomException("No se pudo realizar la venta debido a que no suficiente stock.");
        }
    }
    /**
     * deleteVentas
     * elimina la venta seleccionada
     * @param codigo
     */
    public void deleteVentas(Long codigo){
        VentasEy deleteVentas = repoVentas.findById(codigo).get();
        repoVentas.delete(deleteVentas);
    }
}
