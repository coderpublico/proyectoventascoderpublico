package com.entrega.workshop.service.productos;

import com.entrega.workshop.models.productos.ProductosEy;
import com.entrega.workshop.repository.productos.RepositoryProductos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Service
public class ServiceProductos {
    @Autowired
    private RepositoryProductos repoProductos;

    public String postProductos(ProductosEy productos){
        repoProductos.save(productos);
        return "Producto Guardado";
    }

    public List<ProductosEy> mostrarProductos(){
        List<ProductosEy> lista = repoProductos.findAll();
        return lista;
    }

    public void updateProductos(Long codigo,ProductosEy productos){
        ProductosEy actualizarProducto = repoProductos.findById(codigo).get();
        actualizarProducto.setPrecio(productos.getPrecio());
        actualizarProducto.setDescripcion(productos.getDescripcion());
        actualizarProducto.setStock(productos.getStock());
        repoProductos.save(actualizarProducto);
    }

}
