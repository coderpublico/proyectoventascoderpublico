package com.entrega.workshop.controllers.ventas;
import com.entrega.workshop.models.ventas.VentasEy;
import com.entrega.workshop.service.ventas.ServiceVentas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller de comprobante de ventas
 */
@RestController
public class ControllerVentas {

    @Autowired
    private ServiceVentas srvVentas;

    @GetMapping("api/ventas")
    public List<VentasEy> getVentas(){
        List<VentasEy> listVentas = srvVentas.mostrarVentas();
        return listVentas;
    }
    @GetMapping("api/ventas/{idVenta}")
    public VentasEy getVentaPorId(@PathVariable Long idVenta){
        VentasEy venta = srvVentas.mostrarComprobanteDeVenta(idVenta);
        return venta;
    }
    @PostMapping("api/ventas/alta")
    public String post(@RequestBody VentasEy ventas){
        srvVentas.postVentas(ventas);
        return "Venta Ejecutada";
    }

    @DeleteMapping("api/ventas/baja/{idVenta}")
    public String delete(@PathVariable Long idVenta){
        srvVentas.deleteVentas(idVenta);
        return "Venta Eliminada";
    }

}
