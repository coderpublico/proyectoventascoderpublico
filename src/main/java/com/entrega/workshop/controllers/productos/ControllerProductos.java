package com.entrega.workshop.controllers.productos;
import com.entrega.workshop.models.clientes.ClienteEy;
import com.entrega.workshop.models.productos.ProductosEy;
import com.entrega.workshop.repository.productos.RepositoryProductos;
import com.entrega.workshop.service.clientes.ServiceClientes;
import com.entrega.workshop.service.productos.ServiceProductos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ControllerProductos {
    @Autowired
    private ServiceProductos srvProductos;
    @GetMapping("api/productos")
    public List<ProductosEy> getProductos(){
        List<ProductosEy> listProductos = srvProductos.mostrarProductos();
        return listProductos;
    }

    @PostMapping("api/productos/alta")
    public String post(@RequestBody ProductosEy productos){
        srvProductos.postProductos(productos);
        return "Producto Guardado";
    }

    @PutMapping("api/productos/modificar/{codigo}")
    public String update(@PathVariable Long codigo, @RequestBody ProductosEy productos){
        srvProductos.updateProductos(codigo, productos);
        return "Producto Modificado";
    }

}
