package com.entrega.workshop.controllers.clientes;

import com.entrega.workshop.models.clientes.ClienteEy;
import com.entrega.workshop.service.clientes.ServiceClientes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ControllerClientes {

    @Autowired
    private ServiceClientes srvClientes;
    @GetMapping("api")
    public String index(){
        return "Conectado";
    }

    @GetMapping("api/clientes")
    public List<ClienteEy> getClientes(){
        List<ClienteEy> listClientes = srvClientes.mostrarClientes();
        return listClientes;
    }

    @PostMapping("api/clientes/alta")
    public String post(@RequestBody ClienteEy cliente){
        srvClientes.postCliente(cliente);
       return "Cliente Guardado";
    }

    @PutMapping("api/clientes/modificar/{id}")
    public String update(@PathVariable Long id, @RequestBody ClienteEy cliente){
        srvClientes.updateClientes(id,cliente);
        return "Cliente Modificado";
    }

}