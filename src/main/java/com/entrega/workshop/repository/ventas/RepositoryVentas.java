package com.entrega.workshop.repository.ventas;

import com.entrega.workshop.models.ventas.VentasEy;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository de comprobante de ventas
 */
public interface RepositoryVentas extends JpaRepository<VentasEy, Long> {
}
