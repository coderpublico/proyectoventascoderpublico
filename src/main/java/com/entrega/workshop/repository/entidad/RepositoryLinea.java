package com.entrega.workshop.repository.entidad;

import com.entrega.workshop.models.entidad.LineaEy;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositoryLinea extends JpaRepository<LineaEy, Long> {

}
