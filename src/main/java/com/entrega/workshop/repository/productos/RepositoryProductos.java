package com.entrega.workshop.repository.productos;

import com.entrega.workshop.models.productos.ProductosEy;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositoryProductos extends JpaRepository<ProductosEy, Long> {
}
