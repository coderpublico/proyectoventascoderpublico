package com.entrega.workshop.repository.clientes;

import com.entrega.workshop.models.clientes.ClienteEy;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositoryClientes extends JpaRepository<ClienteEy, Long> {

}
