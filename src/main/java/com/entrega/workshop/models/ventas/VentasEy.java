package com.entrega.workshop.models.ventas;
import com.entrega.workshop.models.clientes.ClienteEy;
import com.entrega.workshop.models.entidad.LineaEy;
import com.entrega.workshop.models.productos.ProductosEy;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Set;

/**
 * Entidad de comprobante de ventas
 */
@Entity
public class VentasEy {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    @Getter
    @Setter
    private Long idVenta;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name="id")
    private ClienteEy idCliente;

    @Column
    @Getter
    @Setter
    private Integer cantidad;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name="idProducto")
    private ProductosEy idProducto;

    @Column
    @Getter
    @Setter
    private Float gastoTotal;

    @Column
    @Getter
    @Setter
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Getter
    @Setter
    @OneToMany(mappedBy = "comprobanteDeVenta")
    @JsonManagedReference
    private Set<LineaEy> lineas;

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Comprobante de Venta [");
        if (idVenta != null)
            builder.append("comprobanteid=").append(idVenta).append(", ");
        if (cantidad != null)
            builder.append("cantidad=").append(cantidad).append(", ");
        if (fecha != null)
            builder.append("fecha=").append(fecha).append(", ");
        if (gastoTotal != null)
            builder.append("total=").append(gastoTotal).append(", ");
        if (idCliente != null)
            builder.append("cliente=").append(idCliente).append(", ");
        if (lineas != null)
            builder.append("lineas=").append(lineas);
        builder.append("]");
        return builder.toString();
    }

}
