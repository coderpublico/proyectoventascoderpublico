package com.entrega.workshop.models.entidad;

import com.entrega.workshop.models.ventas.VentasEy;
import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.JdbcTypeCode;

@Entity
public class LineaEy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Long lineaid;

    @Column
    @Getter
    @Setter
    private Integer cantidad;

    @Column
    @Getter
    @Setter
    private String descripcion;

    @Column
    @Getter
    @Setter
    private Float precio;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "id_venta")
    @JsonBackReference
    private VentasEy comprobanteDeVenta;
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Linea [lineaid=").append(lineaid).append(", cantidad=").append(cantidad).append(", ");
        if (descripcion != null)
            builder.append("descripcion=").append(descripcion).append(", ");
        if (precio != null)
            builder.append("precio=").append(precio);
        builder.append("]");
        return builder.toString();
    }
}
