package com.entrega.workshop.models.entidad;

public class CustomException extends RuntimeException {
    public CustomException(String message) {
        super(message);
    }
}