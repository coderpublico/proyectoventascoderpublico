package com.entrega.workshop.models.clientes;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;


@Entity
public class ClienteEy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Long id;

    @Column
    @Getter
    @Setter
    private String nombre;

    @Column
    @Getter
    @Setter
    private String email;

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Cliente [");
        if (id != null)
            builder.append("clienteid=").append(id).append(", ");
        if (email != null)
            builder.append("email=").append(email);
        builder.append("]");
        return builder.toString();
    }

}
