package com.entrega.workshop.models.productos;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
@Entity
public class ProductosEy {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Long idProducto;

    @Column
    @Getter
    @Setter
    private Integer codigo;

    @Column
    @Getter
    @Setter
    private String descripcion;

    @Column
    @Getter
    @Setter
    private Integer stock;

    @Column
    @Getter
    @Setter
    private Float precio;

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Producto [");
        if (idProducto != null)
            builder.append("productoid=").append(idProducto).append(", ");
        if (stock != null)
            builder.append("cantidad=").append(stock).append(", ");
        if (codigo != null)
            builder.append("codigo=").append(codigo);
        builder.append("]");
        return builder.toString();
    }
}
